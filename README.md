# Rouk Client Api

This is the API client for the rouk collection servers.
The api is used by first generating a public private key pair to identify the client device.

## Transaction types

In the Ruok system there is a number of core transactions that are important to understand.

### registerAppTx
The regiseter App transaction is used to publish the public key of the application to the ledger.


### QuestionsTx

The question transaction is used by an application for register or update the questions that the application will 
publish to the app via the RUOK service. this is this transaction will issue a new version of questions for the 
regisetere application.  

### ReportTx

A report transaction contains the response from a client to a answer set. the response contains a json encoded response 
to a Questions set and a pointer to the referenced question set. 

### MessageTx

The message Transaction are used to anonymously send an end to end encrypted message beetwen devices or between a client 
and an app. 

## core functions
This library contains some of the core functions of the FYEO ledger api and working with FYEO credentials
 - register application
 - register questions
 - register response
 

## basic usage 
the following example initiates a client and sends a set of credentials to the ledger.
The constructor of the RuokClient takes three arguments the first is the dns name of the node to connect to. The second and the third are an application ID and a Application key. 

for use with the test-network 'ledger-1.ruok.foundation' the application id and key can be ommited.

the second  line initialises the keypair for the client with a seed. 

``` typescript jsx

    const rc = new RuokClient('ledger-1.ruok.foundation', '','')
    const sender_keypair:ed25519KeyPair = Ed25519Keypair(Uint8Array.from(seed))

```

to register a new application to the network simply call the register-application functions with the private key of the app and the name of the app you wish to create


to register a new set of questions to your app.

```typescript jsx
const keypair = new driver.Ed25519Keypair(Uint8Array.from(app_seed))
    const questions: QuestionsTx = {date: new Date(), questions:[
        {order: 0, key: 'how', description: 'how are you feeling today then sir', type: 'free'},
        {order: 1, key: 'when', description: 'when did you start feeling like this', type: 'free'},
        {order:2, key: 'other', description: 'any other comments', type: 'free'}
    ]}

    const q_res = await rc.postQuestions(questions, keypair)
```

more examples basic usage can be found in the unit test file.
[index.test.js](https://gitlab.com/f.y.e.o/fyeo-ledger-client/-/blob/master/src/index.test.ts)

