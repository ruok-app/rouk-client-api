"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TransactionTypes = void 0;
var question_types;
(function (question_types) {
    question_types[question_types["bool"] = 0] = "bool";
    question_types[question_types["string"] = 1] = "string";
    question_types[question_types["int"] = 2] = "int";
    question_types[question_types["float"] = 3] = "float";
})(question_types || (question_types = {}));
var TransactionTypes;
(function (TransactionTypes) {
    TransactionTypes[TransactionTypes["reg_app"] = 0] = "reg_app";
    TransactionTypes[TransactionTypes["reg_account"] = 1] = "reg_account";
    TransactionTypes[TransactionTypes["reg_questions"] = 2] = "reg_questions";
    TransactionTypes[TransactionTypes["reg_response"] = 3] = "reg_response";
    TransactionTypes[TransactionTypes["enc_response"] = 4] = "enc_response";
})(TransactionTypes = exports.TransactionTypes || (exports.TransactionTypes = {}));
// interface RegisterPublicKeyX {
//
// }
