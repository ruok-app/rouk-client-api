"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RuokClient = exports.Ed25519Keypair = void 0;
//import * as driver from 'bigchaindb-driver';
// @ts-ignore
const driver = __importStar(require("bigchaindb-driver"));
const Transactions_1 = require("./Transactions");
const tweetnacl_ts_1 = require("tweetnacl-ts");
// @ts-ignore
const bs58_1 = __importDefault(require("bs58"));
function Ed25519Keypair(seed) {
    const sign_pair = tweetnacl_ts_1.sign_keyPair_fromSeed(seed);
    const enc_pair = tweetnacl_ts_1.box_keyPair_fromSecretKey(sign_pair.secretKey.slice(0, 32));
    //tweet-nacls private key also contains a public. just use the private part
    return {
        publicKey: bs58_1.default.encode(sign_pair.publicKey),
        privateKey: bs58_1.default.encode(sign_pair.secretKey.slice(0, 32)),
        encryptionKey: bs58_1.default.encode(enc_pair.publicKey)
    };
}
exports.Ed25519Keypair = Ed25519Keypair;
class RuokClient {
    constructor(server_uri, app_id, app_key) {
        this.path = '/api/v1/';
        this.endpoint = "https://" + server_uri + this.path;
        this.app_id = app_id;
        this.app_key = app_key;
        this.connection = new driver.Connection(this.endpoint);
        //{app_id: this.app_id, app_key: this.app_key
        //})
    }
    set secret_seed(value) {
        this._secret_seed = value;
    }
    postTxFetch(tx) {
        return __awaiter(this, void 0, void 0, function* () {
            const taget_url = this.endpoint + 'transactions/?mode=commit';
            const body = JSON.stringify(tx);
            console.log('[fetch][post]:', taget_url, ' body:', body);
            return fetch(taget_url, {
                method: 'POST',
                headers: {
                    'Accept': '*/*',
                    'Content-Type': 'application/json; charset=utf-8'
                },
                mode: 'no-cors',
                body: body
            })
                .then((res) => __awaiter(this, void 0, void 0, function* () {
                if (res.ok) {
                    return { status: 'ok', code: res.status, response: yield res.json() };
                }
                else {
                    return { status: 'error', code: res.status, response: yield res.json() };
                }
            }))
                .catch((err) => {
                console.log("[fetch] err:", err);
                console.trace();
            });
        });
    }
    //     return fetch(this.endpoint + 'transactions/?mode=commit', {
    //         method: 'POST',
    //         headers: {
    //             'Accept': '*/*',
    //             'Content-Type': 'application/json; charset=utf-8'
    //         },
    //         mode: 'no-cors',
    //         body: JSON.stringify(tx)
    //     })
    //         .then(async (res: any) => {
    //             if (res.ok) {
    //                 return {status: 'ok', code: res.status, response: await res.json()}
    //             } else {
    //                 return {status: 'error', code: res.status, response: await res.json()}
    //             }
    //         })
    //         .catch((err: any) => {
    //             console.log(err)
    //         })
    // }
    sendEncryptedMessage(cleartext_message, sending_keypair, receiving_pub_key) {
        let encrypted_message = this.encryptPayload(cleartext_message, receiving_pub_key, sending_keypair);
        let enc_tx = driver.Transaction.makeCreateTransaction({ message: encrypted_message }, { message: { type: 'encrypted_message', to: receiving_pub_key } }, [driver.Transaction.makeOutput(driver.Transaction.makeEd25519Condition(sending_keypair.publicKey))], sending_keypair.publicKey);
        const txSigned = driver.Transaction.signTransaction(enc_tx, sending_keypair.privateKey);
        return this.postTxFetch(txSigned);
    }
    encryptPayload(cleartext_message, recpt_enc_k, sender_keyPair) {
        const nonce = tweetnacl_ts_1.randomBytes(24 /* Nonce */);
        nonce[0] = 2;
        const bytes_msg = tweetnacl_ts_1.ByteArray(Buffer.from(cleartext_message));
        let encrypted_message = tweetnacl_ts_1.box(bytes_msg, nonce, tweetnacl_ts_1.ByteArray(bs58_1.default.decode(recpt_enc_k)), tweetnacl_ts_1.ByteArray(bs58_1.default.decode(sender_keyPair.privateKey)));
        return {
            message: tweetnacl_ts_1.encodeBase64(encrypted_message),
            nonce: tweetnacl_ts_1.encodeBase64(nonce),
            sender_key: sender_keyPair.encryptionKey
        };
    }
    decryptPayload(message, priv_key) {
        const enc_msg = tweetnacl_ts_1.decodeBase64(message.message);
        const raw_nonce = tweetnacl_ts_1.decodeBase64(message.nonce);
        const public_key = tweetnacl_ts_1.ByteArray(bs58_1.default.decode(message.sender_key));
        const private_key = tweetnacl_ts_1.ByteArray(bs58_1.default.decode(priv_key));
        const decrypted = tweetnacl_ts_1.box_open(enc_msg, raw_nonce, public_key, private_key);
        if (decrypted === undefined) {
            console.log('decryption failed');
            return undefined;
        }
        else {
            return Buffer.from(decrypted).toString('utf-8');
        }
    }
    /**
     * Takes a response transaction and encrypts it and returns an encrypted
     * transaction ready to be sent to the blockchain
     *
     * @param response
     * @param sender_keypair
     * @param app_addr
     * @param encryption_key the public encyption key of the app
     * @private
     */
    encryptResponse(response, sender_keypair, app_addr, encryption_key) {
        const nonce = tweetnacl_ts_1.randomBytes(24 /* Nonce */);
        const bytes_msg = tweetnacl_ts_1.ByteArray(Buffer.from(JSON.stringify(response)));
        let encrypted_message = tweetnacl_ts_1.box(bytes_msg, nonce, tweetnacl_ts_1.ByteArray(bs58_1.default.decode(sender_keypair.encryptionKey)), tweetnacl_ts_1.ByteArray(bs58_1.default.decode(encryption_key)));
        console.log('message:', encrypted_message);
        return {
            type: Transactions_1.TransactionTypes.enc_response,
            content: tweetnacl_ts_1.encodeBase64(encrypted_message),
            nonce: tweetnacl_ts_1.encodeBase64(nonce),
            app: app_addr,
            sender_key: sender_keypair.encryptionKey
        };
    }
    /**
     * Takes an encrypted transaction from the chain and tries to decrypt it and return the unencrypted transaction
     * @param message
     * @param priv_key
     */
    decryptResponseTx(message, priv_key) {
        const enc_msg = tweetnacl_ts_1.decodeBase64(message.content);
        const raw_nonce = tweetnacl_ts_1.decodeBase64(message.nonce);
        const public_key = tweetnacl_ts_1.ByteArray(bs58_1.default.decode(message.sender_key));
        const private_key = tweetnacl_ts_1.ByteArray(bs58_1.default.decode(priv_key));
        const decrypted = tweetnacl_ts_1.box_open(enc_msg, raw_nonce, public_key, private_key);
        if (decrypted === undefined) {
            console.log('decryption failed');
            throw Error('Error decrypting response');
        }
        else {
            let string_data = Buffer.from(decrypted).toString('utf-8');
            return JSON.parse(string_data);
        }
    }
    postMoiUpdate(message, keypair) {
        const tx = driver.Transaction.makeCreateTransaction({ report: message }, { message: {} }, [driver.Transaction.makeOutput(driver.Transaction.makeEd25519Condition(keypair.publicKey))], keypair.publicKey);
        return this.signAndSend(tx, keypair);
    }
    /** sends an encrypted response to the moi network encrypted with the encryption key of the app*/
    encryptedResponse(message, keypair, receiver_public_key) {
        return receiver_public_key;
    }
    postQuestions(message, keypair) {
        return __awaiter(this, void 0, void 0, function* () {
            const data = { message: message };
            const metadata = { message: { type: 'questions', app: keypair.publicKey } };
            const tx = driver.Transaction.makeCreateTransaction(data, metadata, [driver.Transaction.makeOutput(driver.Transaction.makeEd25519Condition(keypair.publicKey))], keypair.publicKey);
            return this.signAndSend(tx, keypair);
        });
    }
    /**
     * signs and sends a transaction to the chain
     * @param tx: unsigned transaction
     * @param keypair the signing keypair
     * @private
     */
    signAndSend(tx, keypair) {
        const signed_tx = driver.Transaction.signTransaction(tx, keypair.privateKey);
        return this.postTxFetch(signed_tx);
    }
    /** Returns the questions to answer by account identifier */
    getQuestionsByPublicKey(public_key) {
        return this.connection.searchMetadata(public_key);
    }
    /**
     * Posts a response to an application
     * @param reponse: A valid response transaction
     * @param app_addres: The public address of the application
     * @param client_keypair: the clients keypair for signing
     * @param app_key an optional app_key used to encrypt the response. if set to false no encryption will occur
     */
    postAppResponse(reponse, app_addres, client_keypair, app_key) {
        return __awaiter(this, void 0, void 0, function* () {
            const metadata = { message: { type: 'response', client: client_keypair.publicKey, app: app_addres } };
            if (typeof (app_key) === 'string') {
                return yield this.EncryptSignSend(reponse, client_keypair, app_addres, app_key);
            }
            else {
                const tx = driver.Transaction.makeCreateTransaction(reponse, metadata, [driver.Transaction.makeOutput(driver.Transaction.makeEd25519Condition(client_keypair.publicKey))], client_keypair.publicKey);
                return yield this.signAndSend(tx, client_keypair);
            }
        });
    }
    EncryptSignSend(reponse, client_keypair, app_addr, app_key) {
        return __awaiter(this, void 0, void 0, function* () {
            let enc_response = this.encryptResponse(reponse, client_keypair, app_addr, app_key);
            let metadata = { message: { app: app_addr, client: client_keypair.publicKey } };
            const data = { message: enc_response };
            const new_tx = driver.Transaction.makeCreateTransaction(data, metadata, [driver.Transaction.makeOutput(driver.Transaction.makeEd25519Condition(client_keypair.publicKey))], client_keypair.publicKey);
            const response_tx = yield this.signAndSend(new_tx, client_keypair);
            return (response_tx);
        });
    }
    registerMoiResponse(accout_seed, response) {
    }
    /**
     * Transfers ownership of a transaction from the client that created the asset or data point to the receiving app
     * @param Transaction
     * @param sender_key
     * @param rcpt_public_key
     * @private
     */
    transferOwnership(Transaction, sender_key, rcpt_public_key) {
        const createTranfer = driver.Transaction.makeTransferTransaction(
        // The output index 0 is the one that is being spent
        [{
                tx: Transaction,
                output_index: 0
            }], [driver.Transaction.makeOutput(driver.Transaction.makeEd25519Condition(rcpt_public_key))], {
            datetime: new Date().toString(),
            value: {
                value: '1',
            }
        });
        return this.signAndSend(createTranfer, sender_key);
    }
    /**
     * Registers an account to the chain and moves the ownership of the account to the receiving app.
     * The account registration also registers the encryption key that can be used to encrypt messages for the account
     * @param rAccountTx
     * @param signing_pair
     */
    registerAccount(rAccountTx, signing_pair) {
        return __awaiter(this, void 0, void 0, function* () {
            const data = { message: rAccountTx };
            const metadata = { message: { type: 'registerAccount', client: signing_pair.publicKey } };
            const create_tx = driver.Transaction.makeCreateTransaction(data, metadata, [driver.Transaction.makeOutput(driver.Transaction.makeEd25519Condition(signing_pair.publicKey))], signing_pair.publicKey);
            const response_tx = yield this.signAndSend(create_tx, signing_pair);
            return this.transferOwnership(response_tx, signing_pair, rAccountTx.account_key);
        });
    }
    /**
     * Registers a new application to the chain.
     * @param appRegister
     * @param app_keypair
     */
    registerApplication(appRegister, app_keypair) {
        return __awaiter(this, void 0, void 0, function* () {
            const data = { message: appRegister };
            const metadata = {
                message: {
                    type: 'app',
                    app_id: appRegister.app_address,
                    app_name: appRegister.app,
                    encryption_key: appRegister.encryption_key
                }
            };
            const new_app_tx = driver.Transaction.makeCreateTransaction(data, metadata, [driver.Transaction.makeOutput(driver.Transaction.makeEd25519Condition(app_keypair.publicKey))], app_keypair.publicKey);
            const response_tx = yield this.signAndSend(new_app_tx, app_keypair);
            return (response_tx);
        });
    }
}
exports.RuokClient = RuokClient;
