import stringMatching = jasmine.stringMatching;

export interface MoiResponse {
    time: string,
    feeling: string
    location: string
}

enum question_types {
    bool,
    string,
    int,
    float
}

export enum TransactionTypes {
    "reg_app",
    "reg_account",
    "reg_questions",
    "reg_response",
    "enc_response"
}

export interface QuestionsTx {
     date: Date,
     questions: {order: number, key: string, description: string, type:string}[]
}

export interface QuestionResponse {
    key: string,
    answer:string,
}

export interface ResponseTx {
    questions_id: string
    date: Date,
    answers: QuestionResponse[]
}

export interface EncryptedResponseTx{
    type: TransactionTypes.enc_response,
    nonce: string, //this nonce is used for the encryption and decryption of the data only. NOT a fyeo credentials nonce
    content: string,
    sender_key: string[32]
    app: string[32]
}

export interface EncryptedMessageTx{
    message: string,
    nonce: string,
    //rcpt_key: string,
    sender_key: string,
}

export interface RegisterApplicationTx {
    app: string
    app_address: string[32]
    encryption_key: string[32]

}

export interface RegisterAccountTx {
    account_key: string[32]
    encryption_key: string[32]
    application_key: string[32]
}

// interface RegisterPublicKeyX {
//
// }

