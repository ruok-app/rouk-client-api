import {Ed25519Keypair, ed25519KeyPair,  RuokClient} from '../index';
import {QuestionsTx, EncryptedMessageTx, QuestionResponse, EncryptedResponseTx} from "../Transactions";
import {toSeed} from "bip39-ts";
import * as driver from 'bigchaindb-driver'
import 'whatwg-fetch'

jest.setTimeout(30000);

const mnemonic:string = 'crew describe parrot approve crumble observe monkey list arch bacon before appear'
const seed = toSeed(mnemonic).slice(0,32);
const seed2 = toSeed('common cabbage eyebrow skin century rule tail heavy cereal judge grape bring').slice(0,32)
const app_seed = toSeed('fine unfold surround evil rapid dwarf chef spin answer spray oval correct south master nurse').slice(0,32)

const rc = new RuokClient('ledger-1.ruok.foundation', '','')

test('moi-report', async() => {
    const rc = new RuokClient('ledger-1.ruok.foundation', '', '')
    const keypair = Ed25519Keypair(Uint8Array.from(seed))
    const res = await rc.postMoiUpdate({time: Date.now().toString(), location: 'test', feeling: 'meh'}, keypair)
    console.log(res)
})

test('the-server-is-up', async() =>{
    const API_PATH = 'https://ledger-1.ruok.foundation/api/v1/'
    const conn = new driver.Connection(API_PATH)

    // const alice = new driver.Ed25519Keypair()
    // const assetdata = {report: {name: 'abcd1234', feeling: 'meh'}}
    // const metadata = {'planet': 'earth'}
    // const txCreateAliceSimple = driver.Transaction.makeCreateTransaction(
    //     assetdata,
    //     metadata,
    //     [ driver.Transaction.makeOutput(driver.Transaction.makeEd25519Condition(alice.publicKey))],
    //     alice.publicKey
    // )
    // const txCreateAliceSimpleSigned = driver.Transaction.signTransaction(txCreateAliceSimple, alice.privateKey)
    const res = await conn.getBlock(1)
    console.log('block', res)
    // const res = await conn.postTransactionCommit(txCreateAliceSimpleSigned)
    console.log(res)
})

test('encrypted-message', () =>{
    const rc = new RuokClient('ledger-1.ruok.foundation', '', '')
    const sender_keypair = Ed25519Keypair(Uint8Array.from(seed))
    const receiver_keypair = Ed25519Keypair(Uint8Array.from(seed))
    const res = rc.sendEncryptedMessage("this is a test message",
        sender_keypair,
        receiver_keypair.publicKey)
})

test ('encrypt-decrypt-message', () => {
    const sender_keypair = Ed25519Keypair(Uint8Array.from(seed))
    const recv_keypair = Ed25519Keypair(Uint8Array.from(seed2))
    const clear_text = "this is a test message"
    const encypted:EncryptedMessageTx = rc.encryptPayload( clear_text, recv_keypair.encryptionKey , sender_keypair)
    console.log('encrypted tx:', JSON.stringify(encypted))
    const decrypted = rc.decryptPayload(encypted, recv_keypair.privateKey)
    expect(decrypted).toEqual(clear_text)
    console.log("decrypted", decrypted)
})

test('encrypt-decrypt-different-keys', () =>{
    const rc = new RuokClient('ledger-1.ruok.foundation', '','')
    const sender_keypair:ed25519KeyPair = Ed25519Keypair(Uint8Array.from(seed))
    const rcpt_keypair:ed25519KeyPair = Ed25519Keypair(Uint8Array.from(seed2))
    const cleat_text:string = 'this is a test message'
    const encrypted_msg = rc.encryptPayload(cleat_text, rcpt_keypair.encryptionKey, sender_keypair)
    console.log(encrypted_msg)
    const decrypted_msg: any = rc.decryptPayload(encrypted_msg, rcpt_keypair.privateKey)
    expect(decrypted_msg).toEqual(cleat_text)
    console.log(decrypted_msg)
})


/**
 * Creates a keypair.
 * Registers the key as a new application to the chain
 * Queries the chain for the application name and returns the key
 */
test( 'register-app', async ()=> {
    const app_keys = Ed25519Keypair(Uint8Array.from(seed))
    rc.registerApplication({
        app: `my test app`,
        app_address: app_keys.publicKey,
        encryption_key: app_keys.encryptionKey
    },
    app_keys)
})

/**
 * Createds a client instance and a keypair.
 * Registers the key as a new application to the chain
 * Queries the chain for the application name and returns the key
 */
test( 'register-account', async () => {
    const sender_keypair:ed25519KeyPair = Ed25519Keypair(Uint8Array.from(seed))
    const app_id = Ed25519Keypair(Uint8Array.from(app_seed)).publicKey
    rc.registerAccount({
        account_key:sender_keypair.publicKey,
        application_key: app_id,
        encryption_key: sender_keypair.encryptionKey }, sender_keypair)
})

/**
 * registers a new set of questions to the application
 */
test( 'register-questions', async  () => {
    const app_keys = Ed25519Keypair(Uint8Array.from(app_seed))
    const questions_tx = {
        date: new Date(),
        questions: [
            {order: 0, key: "how", description: "How are you feeling today?", type: "string"},
            {order: 1, key: 'sleep', description: "how many hours did you sleep tonight?", type: "float"}
        ]
    }
    const questions_id = await rc.postQuestions(questions_tx, app_keys)
})

test('get-questions-by-app-id', async () => {
    const keypair = Ed25519Keypair(Uint8Array.from(app_seed))
    const questions: QuestionsTx = {date: new Date(), questions:[
        {order: 0, key: 'how', description: 'how are you feeling today then sir', type: 'free'},
        {order: 1, key: 'when', description: 'when did you start feeling like this', type: 'free'},
        {order:2, key: 'other', description: 'any other comments', type: 'free'}
    ]}

    const q_res = await rc.postQuestions(questions, keypair)
    console.log(q_res)
    rc.getQuestionsByPublicKey(keypair.publicKey).then( (searchResult: any) => {
            console.log('[search] result:', searchResult)
        })
    })

test('send-encrypted-tx', async() => {
    const keypair = Ed25519Keypair(Uint8Array.from(seed))
    const app_keys = Ed25519Keypair(Uint8Array.from(app_seed))
    let response = {
        questions_id: 'test_set',
        date: new Date(),
        answers: [{key: 'how', answer: 'i feel great'}]
    }
    const encrypted_tx:EncryptedResponseTx = rc.encryptResponse(response, keypair, app_keys.publicKey, app_keys.encryptionKey)
    console.log("enc tx:", JSON.stringify(encrypted_tx))
    //expect(encrypted_tx.hash).toEqual("o2Md3zPaRqensrxTx9w5PKlEvUXlEc/aYeF40eMleOk=")
})

test( 'encrypt-decrypt-response', () => {
    const sender_keypair = Ed25519Keypair(Uint8Array.from(seed))
    const app_keys = Ed25519Keypair(Uint8Array.from(app_seed))
    let response = {
        questions_id: 'test_set',
        date: new Date(),
        answers: [{key: 'how', answer: 'i feel great'}]
    }
    let enc_response = rc.encryptResponse(response, sender_keypair, app_keys.publicKey, app_keys.encryptionKey)
    expect(enc_response.sender_key).toEqual(sender_keypair.encryptionKey)
    expect(enc_response.app).toEqual(app_keys.publicKey)
    console.log("encrypted_message", enc_response)

    let decrypted_response = rc.decryptResponseTx(enc_response, app_keys.privateKey)
    console.log('decrypted', JSON.stringify(decrypted_response))


})

test( 'register-app-response', async () =>{
    const keypair = Ed25519Keypair(Uint8Array.from(seed))
    const app_keys = Ed25519Keypair(Uint8Array.from(app_seed))

    const answers = [{key: 'how', answer: 'i feel great'}]
    rc.postAppResponse({
        questions_id: 'test_set',
        date: new Date(),
        answers: answers
    }, app_keys.publicKey, keypair, app_keys.encryptionKey )
})


