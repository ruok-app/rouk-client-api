//import * as driver from 'bigchaindb-driver';
// @ts-ignore
import * as driver from 'bigchaindb-driver';
import {
    EncryptedMessageTx,
    EncryptedResponseTx,
    MoiResponse,
    QuestionsTx,
    RegisterAccountTx,
    RegisterApplicationTx,
    ResponseTx,
    TransactionTypes
} from "./Transactions";
import {
    box,
    box_keyPair_fromSecretKey,
    box_open,
    BoxKeyPair,
    BoxLength,
    ByteArray,
    decodeBase64,
    encodeBase64,
    randomBytes,
    sign_keyPair_fromSeed,
    SignKeyPair
} from 'tweetnacl-ts';
// @ts-ignore
import base58 from 'bs58'

// @ts-ignore

export interface ed25519KeyPair{
    //base58 encdoded representation to be compatible with bigcahindb key pairs
    privateKey: string
    publicKey: string, //the public key against which messages are verified
    encryptionKey: string[32], //this is an optional public key for message encryption on chain and this key is using
}


export function Ed25519Keypair(seed:Uint8Array): ed25519KeyPair{
    const sign_pair: SignKeyPair = sign_keyPair_fromSeed(seed)
    const enc_pair: BoxKeyPair = box_keyPair_fromSecretKey(sign_pair.secretKey.slice(0, 32))
    //tweet-nacls private key also contains a public. just use the private part

    return {
        publicKey: base58.encode(sign_pair.publicKey),
        privateKey: base58.encode(sign_pair.secretKey.slice(0, 32)),
        encryptionKey: base58.encode(enc_pair.publicKey)
    }
}

export class RuokClient {
    set secret_seed(value: string) {
        this._secret_seed = value;
    }

    endpoint: string;
    private path = '/api/v1/'
    private connection: any;
    private app_id: string
    private app_key: string
    private _secret_seed!: string;

    constructor(server_uri: string ,app_id: string, app_key: string) {
        this.endpoint = "https://" + server_uri + this.path
        this.app_id = app_id
        this.app_key = app_key
        this.connection = new driver.Connection(this.endpoint)
            //{app_id: this.app_id, app_key: this.app_key
        //})
    }

    async postTxFetch(tx: any): Promise<any> {
        const taget_url = this.endpoint + 'transactions/?mode=commit'
        const body = JSON.stringify(tx)
        console.log('[fetch][post]:', taget_url, ' body:', body)
        return fetch(taget_url, {
            method: 'POST',
            headers: {
                'Accept': '*/*',
                'Content-Type': 'application/json; charset=utf-8'
            },
            mode: 'no-cors',
            body: body
        })
            .then(async (res: any) => {
                if (res.ok) {
                    return {status: 'ok', code: res.status, response: await res.json()}
                } else {
                    return {status: 'error', code: res.status, response: await res.json()}
                }
            })
            .catch((err: any) => {
                console.log("[fetch] err:", err)
                console.trace();
            })

    }
    //     return fetch(this.endpoint + 'transactions/?mode=commit', {
    //         method: 'POST',
    //         headers: {
    //             'Accept': '*/*',
    //             'Content-Type': 'application/json; charset=utf-8'
    //         },
    //         mode: 'no-cors',
    //         body: JSON.stringify(tx)
    //     })
    //         .then(async (res: any) => {
    //             if (res.ok) {
    //                 return {status: 'ok', code: res.status, response: await res.json()}
    //             } else {
    //                 return {status: 'error', code: res.status, response: await res.json()}
    //             }
    //         })
    //         .catch((err: any) => {
    //             console.log(err)
    //         })
    // }

    sendEncryptedMessage(cleartext_message: string, sending_keypair: ed25519KeyPair, receiving_pub_key: string): Promise<string | boolean>{
        let encrypted_message = this.encryptPayload(cleartext_message, receiving_pub_key, sending_keypair);

        let enc_tx = driver.Transaction.makeCreateTransaction({message: encrypted_message},
                                                            {message:{type: 'encrypted_message', to:receiving_pub_key}},
            [driver.Transaction.makeOutput(driver.Transaction.makeEd25519Condition(sending_keypair.publicKey))],
            sending_keypair.publicKey)
        const txSigned = driver.Transaction.signTransaction(enc_tx, sending_keypair.privateKey)
        return this.postTxFetch(txSigned)
    }


    encryptPayload(cleartext_message: string, recpt_enc_k: string, sender_keyPair: ed25519KeyPair): EncryptedMessageTx {
        const nonce = randomBytes(BoxLength.Nonce);
        nonce[0] = 2

        const bytes_msg = ByteArray(Buffer.from(cleartext_message));
        let encrypted_message: Uint8Array = box(
            bytes_msg,
            nonce,
            ByteArray(base58.decode(recpt_enc_k)),
            ByteArray(base58.decode(sender_keyPair.privateKey))
        )
        return {
            message: encodeBase64(encrypted_message),
            nonce: encodeBase64(nonce),
            sender_key: sender_keyPair.encryptionKey}
    }

    decryptPayload(message: EncryptedMessageTx, priv_key:string): string|undefined {
        const enc_msg = decodeBase64(message.message)
        const raw_nonce = decodeBase64(message.nonce)
        const public_key = ByteArray(base58.decode(message.sender_key))
        const private_key = ByteArray(base58.decode(priv_key))
        const decrypted:any = box_open(enc_msg, raw_nonce, public_key, private_key)
        if (decrypted === undefined){
            console.log('decryption failed')
            return undefined
        }
        else{
            return Buffer.from(decrypted).toString('utf-8')
        }
    }


    /**
     * Takes a response transaction and encrypts it and returns an encrypted
     * transaction ready to be sent to the blockchain
     *
     * @param response
     * @param sender_keypair
     * @param app_addr
     * @param encryption_key the public encyption key of the app
     * @private
     */
    encryptResponse(response:ResponseTx,
                    sender_keypair: ed25519KeyPair,
                    app_addr: string[32],
                    encryption_key: string[32]): EncryptedResponseTx {
        const nonce = randomBytes(BoxLength.Nonce);
        const bytes_msg = ByteArray(Buffer.from(JSON.stringify(response)));
        let encrypted_message: Uint8Array = box(
            bytes_msg,
            nonce,
            ByteArray(base58.decode(sender_keypair.encryptionKey)),
            ByteArray(base58.decode(encryption_key))
        )
        console.log('message:', encrypted_message)

        return {
            type: TransactionTypes.enc_response,
            content: encodeBase64(encrypted_message),
            nonce: encodeBase64(nonce),
            app: app_addr,
            sender_key: sender_keypair.encryptionKey}
    }

    /**
     * Takes an encrypted transaction from the chain and tries to decrypt it and return the unencrypted transaction
     * @param message
     * @param priv_key
     */
    decryptResponseTx(message: EncryptedResponseTx, priv_key:string[32]): ResponseTx {
        const enc_msg = decodeBase64(message.content)
        const raw_nonce = decodeBase64(message.nonce)
        const public_key = ByteArray(base58.decode(message.sender_key))
        const private_key = ByteArray(base58.decode(priv_key))
        const decrypted:any = box_open(enc_msg, raw_nonce, public_key, private_key)


        if (decrypted === undefined){
            console.log('decryption failed')
            throw Error('Error decrypting response')
        }
        else{
            let string_data = Buffer.from(decrypted).toString('utf-8')
            return JSON.parse(string_data)
        }
    }

    postMoiUpdate(message: MoiResponse, keypair: ed25519KeyPair): Promise<any> {
        const tx = driver.Transaction.makeCreateTransaction({report:message}, {message:{}},
            [driver.Transaction.makeOutput(driver.Transaction.makeEd25519Condition(keypair.publicKey))],
            keypair.publicKey
        );
        return this.signAndSend(tx, keypair)
    }

    /** sends an encrypted response to the moi network encrypted with the encryption key of the app*/
    encryptedResponse(message: string, keypair: any, receiver_public_key: any): Promise<any> {
        return receiver_public_key

    }

    async postQuestions(message: QuestionsTx, keypair: ed25519KeyPair): Promise<any>{
        const data = {message:message}
        const metadata = {message: {type: 'questions', app: keypair.publicKey}}
        const tx = driver.Transaction.makeCreateTransaction(data, metadata,
            [driver.Transaction.makeOutput(driver.Transaction.makeEd25519Condition(keypair.publicKey))],
            keypair.publicKey
        );
        return this.signAndSend(tx, keypair);
    }

    /**
     * signs and sends a transaction to the chain
     * @param tx: unsigned transaction
     * @param keypair the signing keypair
     * @private
     */
    private signAndSend(tx: any, keypair: ed25519KeyPair) {
        const signed_tx = driver.Transaction.signTransaction(tx, keypair.privateKey)
        return this.postTxFetch(signed_tx)
    }

    /** Returns the questions to answer by account identifier */
    getQuestionsByPublicKey(public_key: string){
        return this.connection.searchMetadata(public_key)

    }

    /**
     * Posts a response to an application
     * @param reponse: A valid response transaction
     * @param app_addres: The public address of the application
     * @param client_keypair: the clients keypair for signing
     * @param app_key an optional app_key used to encrypt the response. if set to false no encryption will occur
     */
    async postAppResponse(reponse: ResponseTx,
                          app_addres: string[32],
                          client_keypair: ed25519KeyPair,
                          app_key: string[32]|boolean) {
        const metadata = {message: {type: 'response', client: client_keypair.publicKey, app: app_addres}}

        if (typeof (app_key) === 'string'){
            return await this.EncryptSignSend(reponse, client_keypair, app_addres, app_key);
        }
        else {
            const tx = driver.Transaction.makeCreateTransaction(reponse, metadata,
                [driver.Transaction.makeOutput(driver.Transaction.makeEd25519Condition(client_keypair.publicKey))],
                client_keypair.publicKey
            );
            return await this.signAndSend(tx, client_keypair)
        }
    }

    private async EncryptSignSend(reponse: ResponseTx,
                                  client_keypair: ed25519KeyPair,
                                  app_addr : string[32],
                                  app_key: string[32]) {
        let enc_response = this.encryptResponse(reponse, client_keypair, app_addr, app_key)
        let metadata =  { message: { app: app_addr, client: client_keypair.publicKey} }
        const data = {message: enc_response}
        const new_tx = driver.Transaction.makeCreateTransaction(data, metadata,
            [driver.Transaction.makeOutput(driver.Transaction.makeEd25519Condition(client_keypair.publicKey))],
            client_keypair.publicKey
        );
        const response_tx = await this.signAndSend(new_tx, client_keypair)
        return (response_tx)
    }

    registerMoiResponse(accout_seed: Uint8Array, response: MoiResponse){

    }


    /**
     * Transfers ownership of a transaction from the client that created the asset or data point to the receiving app
     * @param Transaction
     * @param sender_key
     * @param rcpt_public_key
     * @private
     */
    private transferOwnership(Transaction: any, sender_key: ed25519KeyPair, rcpt_public_key:string) {
        const createTranfer = driver.Transaction.makeTransferTransaction(
            // The output index 0 is the one that is being spent
            [{
                tx: Transaction,
                output_index: 0
            }],
            [driver.Transaction.makeOutput(driver.Transaction.makeEd25519Condition(rcpt_public_key))],
            {
                datetime: new Date().toString(),
                value: {
                    value: '1',
                }
            }
        )
        return this.signAndSend(createTranfer, sender_key)
    }

    /**
     * Registers an account to the chain and moves the ownership of the account to the receiving app.
     * The account registration also registers the encryption key that can be used to encrypt messages for the account
     * @param rAccountTx
     * @param signing_pair
     */

    async registerAccount(rAccountTx: RegisterAccountTx, signing_pair: ed25519KeyPair){
        const data = {message: rAccountTx}
        const metadata = {message: {type: 'registerAccount', client: signing_pair.publicKey}}
        const create_tx = driver.Transaction.makeCreateTransaction(data, metadata,
            [driver.Transaction.makeOutput(driver.Transaction.makeEd25519Condition(signing_pair.publicKey))],
            signing_pair.publicKey
        );
        const response_tx = await this.signAndSend(create_tx, signing_pair)
        return this.transferOwnership(response_tx, signing_pair, rAccountTx.account_key)
    }

    /**
     * Registers a new application to the chain.
     * @param appRegister
     * @param app_keypair
     */
    async registerApplication(appRegister: RegisterApplicationTx, app_keypair: ed25519KeyPair){
        const data = {message: appRegister}
        const metadata = {
            message:
                {
                    type: 'app',
                    app_id: appRegister.app_address,
                    app_name: appRegister.app,
                    encryption_key: appRegister.encryption_key
                }
        }
        const new_app_tx = driver.Transaction.makeCreateTransaction(data, metadata,
            [driver.Transaction.makeOutput(driver.Transaction.makeEd25519Condition(app_keypair.publicKey))],
            app_keypair.publicKey
        );
        const response_tx = await this.signAndSend(new_app_tx, app_keypair)
        return (response_tx)
    }

}
